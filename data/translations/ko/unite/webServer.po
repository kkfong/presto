# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-09-02 10:22-02:00\n"
"PO-Revision-Date: 2009-11-09 14:54+0100\n"
"Last-Translator: Anne Lilleholt\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"

#. Error page title text when a resource is not found
#: templates/fileSharing.html
msgid "Folder or file not found"
msgstr "폴더 또는 파일을 찾을 수 없음"

#. A table header that describes the access level for a file in the selected folder.
#: templates/fileSharing.html
msgid "Access"
msgstr "액세스"

#. A table header that describes the name of a file in the selected folder.
#: templates/fileSharing.html
msgid "Name"
msgstr "이름"

#. A table header that describes the size of a file in the selected folder.
#: templates/fileSharing.html
msgid "Size"
msgstr "크기"

#. A table header that describes the time a file last got modified in the selected folder.
#: templates/fileSharing.html
msgid "Time"
msgstr "시간"

#. A link for a visitor to download a file from the owner's selected folder.
#: templates/fileSharing.html
msgid "Download"
msgstr "다운로드"

#. Singular case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "1 folder"
msgstr "폴더 1개"

#. Plural case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "{counter} folders"
msgstr "폴더 {counter}개"

#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "and"
msgstr "및"

#. Singular case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "1 file"
msgstr "파일 1개"

#. Plural case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "{counter} files"
msgstr "파일 {counter}개"

#. Text displayed when there is no index.html file in the owner's Web Server folder.
#: templates/messages.html
msgid "Visitors will see the files in this folder, as there is no index.html file to display."
msgstr "표시할 index.html 파일이 없으므로 방문자가 이 폴더에서 파일을 보게 됩니다."

#. Text displayed when there is no index.html file in the owner's Web Server folder.
#: templates/messages.html
msgid "If you want visitors to see an index page, create an index.html file or <A href=\"?create_index=true\">generate a sample file</A>."
msgstr "방문자가 색인 페이지를 보게 하려면 index.html 파일을 만들거나 <A href=\"?create_index=true\">샘플 파일을 생성하십시오</A>."

#. Text displayed when there is an index.html file in the owner's Web Server folder.
#: templates/messages.html
msgid "<EM>This folder contains an index.html file.</EM> This is the first page visitors to your Web Server will see: <A href=\"{index}\">{index}</A>"
msgstr "<EM>이 폴더에 index.html 파일이 들어 있습니다.</EM> 웹 서버의 방문자가 보는 첫 페이지는 <A href=\"{index}\">{index}</A>입니다."

#. Message shown when the original share folder selected by the owner can't be accessed
#. Properties... text comes from the right-click menu of the application in the Unite panel.
#: templates/noSharedMountpoint.html
msgid "Folder not found. To select a new one, right-click <STRONG>{serviceName}</STRONG> in the Unite panel, and choose <STRONG>Properties</STRONG>"
msgstr "폴더를 찾을 수 없습니다. 새 폴더를 선택하려면 Unite 패널에서 <STRONG>{serviceName}</STRONG>을(를) 마우스 오른쪽 단추로 클릭하고 <STRONG>속성</STRONG>을 선택하십시오."

#. Text in the generated index.html file.
#: templates/index.html
msgid "This sample Web page <STRONG>index.html</STRONG> was created when you clicked \"generate a sample file\" in a folder without an index.html file. Edit it to suit your taste. This is the first page visitors to your Web Server will see."
msgstr "이 샘플 웹 페이지 <STRONG>index.html</STRONG>은 폴더에서 index.html 파일 없이 \"샘플 폴더 생성\"을 클릭하여 만들어졌습니다. 기호에 맞도록 편집하십시오. 이 페이지는 웹 서버의 방문자가 보게 되는 첫 번째 페이지입니다."

#. A header in the generated index.html that describes a section of the page
#. for the viewer to get resources to learn Web development.
#: templates/index.html
msgid "Resources"
msgstr "리소스"

#. Text in the generated index.html file. Followed by a link to the Opera Web Standards Curriculum.
#: templates/index.html
msgid "To learn more about Web development and design, see the"
msgstr "웹 개발 및 디자인에 대해 더 알아보려면 다음을 참조하십시오."

